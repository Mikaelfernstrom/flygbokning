import java.util.Scanner;


public class Admin { // Admin class h�r g�rs allt i admin menyn.

	static Scanner input = new Scanner(System.in);

	
	public void adminOptions() { // Metod f�r huvud admin menyn.
		MainMenu mainMenu = new MainMenu();

		System.out
				.println("Admin Menu:\n1. Create\n2. Delete\n3. Search\n4. Main menu");
		int userIn = input.nextInt();

		do {
			switch (userIn) {
			case 1:
				create();
				break;
			case 2:
				delete();
				break;

			case 3:
				search();
				break;

			case 4:
				mainMenu.startMenu();
				break;
				
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				adminOptions();
				break;
			}
		} while (userIn >= 4);

	}

	
	public void create() { // metod f�r botten menyn som ska "Creata" Airport, Airline, Flight.

		System.out
				.println("Create:\n1. Create Airport\n2. Create Airline\n3. Create Flight\n4. Back");
		int userIn = input.nextInt();

		do {
			switch (userIn) {
			case 1:
				createAirport();
				break;
			case 2:
				createAirline();
				break;

			case 3:
				createFlight();
				break;

			case 4:
				adminOptions();
				break;
				
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				create();
				break;
			}
		} while (userIn >= 5);

	}

	
	public void delete() { // Metod f�r botten menyn som ska navigera genom olika delete funktioner.
		System.out
				.println("Delete:\n1. Delete airport\n2. Delete airline\n3. Delete flight\n4. Back");
		int userIn = input.nextInt();

		do {
			switch (userIn) {
			case 1:
				deleteAirport();
				break;
			case 2:
				deleteAirline();
				break;

			case 3:
				deleteFlight();
				break;

			case 4:
				adminOptions();
				break;
				
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				delete();
				break;
			}
		} while (userIn >= 4);
	}

	
	public void search() { // Meny till admins s�kfunktion.
		DatabaseConnector setup = new DatabaseConnector();
		System.out
				.println("Search flight:\n1. Search by departure:\n2. Search by destination\n3. Back");

		
		int userIn = input.nextInt();
		do {
			
			switch (userIn) {
			case 1:
				System.out.print("Enter departure location: ");
				setup.departureSearch();
				System.out.println("\n1. New search\n2. Admin menu ");
				backSearch();
				break;
				
			case 2:
				System.out.print("Enter Destination: ");
				setup.destinationSearch();
				System.out.println("\n1. New search\n2. Admin menu ");
				backSearch();
				break;

			case 3:
				adminOptions();
				break;
				
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				search();
				break;

			}
		} while (userIn >= 4);
	}
	
	public void createAirport() { // Metod med f�r skapandet av Airports i databas
		String airIn;
		String location;
		String country;
		int userIn;
		DatabaseConnector airportIn = new DatabaseConnector();
		do {
			System.out.println("1. Add\n2. Back");
			userIn = input.nextInt();
			switch (userIn) 
			{
			case 1:
				System.out
						.println("Create an airportcode that contains 3 letters: ");

				do {
					airIn = input.next();
					if (!Regex.regexAirport(airIn)) {
						System.out.println("Invalid input please try again");
					}
				} while (!Regex.regexAirport(airIn));

				System.out.print("Enter city: ");
				location = input.next();
				System.out.print("Enter contry: ");
				country = input.next();
				Airport airport = new Airport(airIn,location,country);
				airportIn.airportInsert(airport.getName(), airport.getLocation(), airport.getCountry());
				break;
				
			case 2:
				create();
				break;
				
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				createAirport();
				break;
			}

		} while (!(userIn >= 2));

	}
	
	public void createAirline() { // Metod f�r att skapa airlines.
		String airIn;
		String country;
		int userIn;
		DatabaseConnector airlineIn = new DatabaseConnector();
		do {
			System.out.println("1. Add\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out.print("Create an airline that contains a minimum of 6 signs: ");

				do {
					airIn = input.next();
					airIn = airIn + input.nextLine();
					if (!Regex.regexAirline(airIn)) {
						System.out.println("Invalid input please try again");
					}
				} while (!Regex.regexAirline(airIn));
				System.out.print("Set airline country: ");
				country = input.next();
				Airline airline = new Airline(airIn,country);
				airlineIn.airlineInsert(airline.getName(), airline.getLocation());
				break;
			case 2:
				create();
				break;
				
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				createAirline();
				break;
			}

		} while (!(userIn >= 2));
	}
	
	public void createFlight() { // Metod f�r att skapa nya flights mellan olika destinationer. 
		
		String flightName;
		String date;
		int airlineIn;
		int userIn;
		int departureIn;
		int destinationIn;
		DatabaseConnector flightIn = new DatabaseConnector();
		DatabaseConnector showAirline = new DatabaseConnector();
		DatabaseConnector showAirport = new DatabaseConnector();
		do {
			System.out.println("1. New flight\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				showAirline.showAirline();
				System.out.print("Pick Airline: ");
				airlineIn = input.nextInt();
				System.out.println("Set Flight name (SK***): ");
				flightName = input.next();
				showAirport.showAirport();
				System.out.println("Select departure airport: ");
				departureIn = input.nextInt();
				showAirport.showAirport();
				System.out.println("Select destination airport: ");
				destinationIn = input.nextInt();
				System.out.print("Set date (yyyy-mm-dd): ");
                do {
                        date = input.next();
                        if (!Regex.regexDate(date)) {
                                System.out.println("Invalid input please try again");
                        }
                } while (!Regex.regexDate(date));
				//Flight flight = new Flight(airlineIn, flightName, departureIn, destinationIn);
				flightIn.flightInsert(airlineIn, flightName, departureIn, destinationIn, date);
				
				break;
			case 2:
				create();
				break;
				
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				createFlight();
				break;
			}

		} while (!(userIn >= 2));
	}
	
	public void deleteAirport() { // Metod f�r f�r meny till deletfuntioner av airports.
		int userIn;
		DatabaseConnector showAirport = new DatabaseConnector();
		DatabaseConnector delAirport = new DatabaseConnector();

		do {
			showAirport.showAirport();
			System.out.println("\n1. delete\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out
						.print("Delete an airport\nType the number of the airport you want to delete: ");
				delAirport.delAirport();
				break;
				
			case 2:
				delete();
				break;
				
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				deleteAirport();
				break;
			}

		} while (!(userIn >= 2));

	}
	
	public void deleteAirline() { // metod f�r meny val av delete airlines
		int userIn;
		DatabaseConnector showAirline = new DatabaseConnector();
		DatabaseConnector delAirline = new DatabaseConnector();

		do {
			showAirline.showAirline();
			System.out.println("\n1. delete\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out
						.print ("Delete an airline\nType the number of the airline you want to delete: ");
				delAirline.delAirline();
				System.out.println("Deleted!");
				break;
				
			case 2:
				delete();
				break;
			
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				deleteAirline();
				break;
			}

		} while (!(userIn >= 2));
	}
	
	public void deleteFlight() { // Metod f�r delete menyn med val av Flights
		int userIn;
		DatabaseConnector showFlight = new DatabaseConnector();
		DatabaseConnector delFlight = new DatabaseConnector();

		do {
			showFlight.showFlight();
			System.out.println("\n1. delete\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out
						.print("Delete an flight\nType the number of the flight you want to delete: ");
				delFlight.delFlight();
				System.out.println("Deleted!");
				break;
				
			case 2:
				delete();
				break;
				
			default:
				System.out.println("Sorry, that�s not a correct choice! Try again:\n");
				deleteFlight();
				break;
			}

		} while (!(userIn >= 2));
	}
	
	public void backSearch() { // Metod som har funktion f�r att backa i en av s�kmenyerna.

		int userIn;

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				search();
				break;
			case 2:
				adminOptions();
				break;

			}
		} while (userIn >= 1);

	}
}