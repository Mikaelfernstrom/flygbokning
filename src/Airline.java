public class Airline  {
	

	private String name;
	private String location;
	private int id;

	public Airline(String name, String location) {

		this.name = name;
		this.location = location;
	}

	public Airline(String name, String location, int id) {

		this.name = name;
		this.location = location;
		this.id = id;

	}

	public String getName() {

		return this.name;
	}

	public String getLocation() {

		return this.location;
	}

	public int getID() {

		return this.id;
	}
}