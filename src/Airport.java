public class Airport {

	private String name;
	private String location;
	private String country;
	private int id;

	public Airport(String name, String location, String country) {

		this.name = name;
		this.location = location;
		this.country = country;
	}

	public Airport(String name, String location, String country, int id) {

		this.name = name;
		this.location = location;
		this.country = country;
		this.id = id;

	}

	public String getName() {

		return this.name;
	}

	public String getLocation() {

		return this.location;
	}

	public String getCountry() {

		return this.country;
	}

	public int getID() {

		return this.id;
	}
}
